import React from 'react';
import { StyleSheet, View } from 'react-native';

export const PositionScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.cajaVerde} />
      <View style={styles.cajaMorada} />
      <View style={styles.cajaNaranja} />
    </View>
  );
};


const styles = StyleSheet.create({
    container: {
      //flex: 1,
      //width: 400,
      height: 400,
      backgroundColor: '#28C4D9',
      // justifyContent: 'center',
      // alignItems: 'center',
    },
    cajaMorada: {
      width: 100,
      height: 100,
      backgroundColor: '#5856D6',
      borderColor: 'white',
      borderWidth: 10,
      position: 'absolute',
      top: 10,
      right: 10,
    },
    cajaNaranja: {
      width: 100,
      height: 100,
      backgroundColor: '#F0A23B',
      borderColor: 'white',
      borderWidth: 10,
      position: 'absolute',
      bottom: 10,
      right: 10,
    },
    cajaVerde: {
      // width: 100,
      // height: 100,
      backgroundColor: 'green',
      borderColor: 'white',
      borderWidth: 10,
      // position: 'absolute',
      // bottom: 0,
      // left: 0,
      // top: 0,
      // right: 0,
      ...StyleSheet.absoluteFillObject,
    },
});
