import React from 'react';
import { StyleSheet, Text, TouchableNativeFeedback, Platform, TouchableOpacity, View } from 'react-native';

type FabProps = {
  title: string;
  direction?: 'left' | 'right';
  onPress: () => void;
};

export const Fab = ({ title, onPress, direction = 'right' }: FabProps) => {

  const ios = () => {
    return (
      <TouchableOpacity
        activeOpacity={0.75}
        style={ [styles.fabLocation, styles[direction]] }
        onPress={ onPress }
      >
        <View style={ styles.fab }>
          <Text style={ styles.fabText }>{title}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const android = () => {
    return (
      <View style={ [styles.fabLocation, styles[direction]] }>
        <TouchableNativeFeedback
            onPress={ onPress }
            background={TouchableNativeFeedback.Ripple('#28425B', false, 30)}
          >
            <View style={ styles.fab }>
              <Text style={ styles.fabText }>{title}</Text>
            </View>
          </TouchableNativeFeedback>
      </View>
    );
  };


  return (Platform.OS === 'ios') ? ios() : android();
};


const styles = StyleSheet.create({
  fabLocation: {
    position: 'absolute',
    bottom: 10,
  },
  left: {
    left: 10,
  },
  right: {
    right: 10,
  },
  fab: {
    backgroundColor: '#5856D6',
    height: 60,
    width: 60,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor:'#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 4,
  },
  fabText: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});
